//
//  ViewController.swift
//  siriTool
//
//  Created by Alejandro Castillejo on 6/29/19.
//  Copyright © 2019 Alejandro Castillejo. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.title = "VUI Testing Tool"
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(addTapped))
        
        tableView.delegate = self
        tableView.dataSource = self
        
    }
    
    @objc func addTapped() {
        
        let alert = UIAlertController(title: "Add Utterance", message: "", preferredStyle: UIAlertController.Style.alert)
        
        alert.addTextField(configurationHandler: { (textField) in
            textField.placeholder = ""
        })
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler:{ (UIAlertAction) in
            
            if let textFieldText = alert.textFields?[0].text {
                UtteranceController.shared.addUtterance(Utterance(textFieldText))
                self.tableView.reloadData()
            }
            
        }))
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
        
    }
    
    @IBAction func buttonWasTapped(_ button: UIButton) {
        
        if button.tag == 50 {
            SiriController.shared.playSound(SiriSound.start)
        } else if button.tag == 51 {
            SiriController.shared.playSound(SiriSound.end)
        } else if button.tag == 52 {
            SiriController.shared.playSound(SiriSound.error)
        }
        
    }

}

extension ViewController:  UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return UtteranceController.shared.utterances.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellID")
        
        let utterance = UtteranceController.shared.utterances[indexPath.row]
        
        cell?.textLabel?.numberOfLines = 0
        cell?.textLabel?.text = utterance.string

        return cell!
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let utterance = UtteranceController.shared.utterances[indexPath.row]
        
        SiriController.shared.playUtterance(utterance)
        
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            let utterances = UtteranceController.shared.utterances
            UtteranceController.shared.removeUtterance(utterances[indexPath.row])
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
        
    }
    
}
