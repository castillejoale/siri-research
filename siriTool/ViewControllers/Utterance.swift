//
//  Response.swift
//  siriTool
//
//  Created by Alejandro Castillejo on 6/29/19.
//  Copyright © 2019 Alejandro Castillejo. All rights reserved.
//

import Foundation

struct Utterance {
    
    let string: String?
    
    init(string: String? = nil) {
        self.string = string
    }
    
}
