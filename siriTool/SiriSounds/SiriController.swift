//
//  SiriController.swift
//  siriTool
//
//  Created by Alejandro Castillejo on 6/29/19.
//  Copyright © 2019 Alejandro Castillejo. All rights reserved.
//

import Foundation
import AVFoundation

class SiriController {
    
    static let shared = SiriController()
    
    var player: AVAudioPlayer?
 
    func playSound(_ sound: SiriSound) {
        
        guard let url = Bundle.main.url(forResource: sound.rawValue, withExtension: "mp3") else { return }
        
        do {
            
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default)
            try AVAudioSession.sharedInstance().setActive(true)

            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)
            
            guard let player = player else { return }
            
            player.play()
            
        } catch let error {
            print(error.localizedDescription)
        }
        
    }
    
    func playUtterance(_ utterance: Utterance) {
        
        let voices = AVSpeechSynthesisVoice.speechVoices()
        
        var voice:AVSpeechSynthesisVoice
        
        //Get Siri's voice
        if #available(iOS 13, *) {
            voice = voices[17]
        } else {
            voice = voices[8]
        }

        let utterance = AVSpeechUtterance(string: utterance.string)
        utterance.voice = voice
        utterance.rate = 0.5
        
        let synthesizer = AVSpeechSynthesizer()
        synthesizer.speak(utterance)
        
    }
    
}
