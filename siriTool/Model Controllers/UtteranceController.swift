//
//  UtteranceController.swift
//  siriTool
//
//  Created by Alejandro Castillejo on 6/30/19.
//  Copyright © 2019 Alejandro Castillejo. All rights reserved.
//

import Foundation

class UtteranceController {
    
    static let shared = UtteranceController()

    var utterances: [Utterance] = []
    
    private init() {
        utterances = readAllUtterances()
    }
    
    private func generateDefaultUtterances() -> [Utterance] {
        
        var utterances: [Utterance] = []
        
        utterances.append(Utterance("The A string is slightly flat. Want to tune it?"))
        utterances.append(Utterance("Up"))
        utterances.append(Utterance("Down"))
        utterances.append(Utterance("Done, your guitar is tuned"))
        utterances.append(Utterance("Summer Jam loop saved, want me to play it now?"))
        utterances.append(Utterance("It's a C note"))
        utterances.append(Utterance("Let's do this"))
        utterances.append(Utterance("Melody recorded, how do you want to call it?"))
        utterances.append(Utterance("My melody saved"))
        utterances.append(Utterance("The song is called Let it be, sang by Alicia Keys and John Legend at the Webmley concert in 2014"))
        
        return utterances
        
    }
    
    func addUtterance(_ utterance: Utterance) {
        utterances.append(utterance)
    }
    
    func removeUtterance(_ utterance: Utterance) {
        
        for i in 0..<utterances.count {
            if utterance == utterances[i] {
                utterances.remove(at: i)
                break
            }
        }
        
    }
    
    func readAllUtterances() -> [Utterance] {
        
        let defaultUtterances = self.generateDefaultUtterances()
        return defaultUtterances
        
    }

}
