//
//  Utterance.swift
//  siriTool
//
//  Created by Alejandro Castillejo on 6/29/19.
//  Copyright © 2019 Alejandro Castillejo. All rights reserved.
//

import Foundation


class Utterance: NSObject {
    
    let string: String
    
    required init(_ string: String) {
        self.string = string
    }
    
}
