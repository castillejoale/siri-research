//
//  Sound.swift
//  siriTool
//
//  Created by Alejandro Castillejo on 6/29/19.
//  Copyright © 2019 Alejandro Castillejo. All rights reserved.
//

import Foundation

enum SiriSound: String {
    
    case end = "siriEnd"
    case error = "siriError"
    case start = "siriStart"
    
}
